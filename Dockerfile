FROM openjdk:8-jdk-alpine

VOLUME /tmp
ADD /target/poste.jar app.jar

EXPOSE 8181

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-Dspring.profiles.active=prod","-jar","/app.jar"]