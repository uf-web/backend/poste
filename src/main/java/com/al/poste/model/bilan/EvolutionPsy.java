package com.al.poste.model.bilan;

/**
 * Evolution de l’état psy
 */
public enum EvolutionPsy {
    SansChangement, Amelioration, Aggravation
}
