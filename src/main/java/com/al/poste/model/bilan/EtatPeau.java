package com.al.poste.model.bilan;

/**
 * Etat de la Peau
 */
public enum EtatPeau {
    /**
     * Normal : Normale
     * Pale : Pâle
     * Marbre : Marbrée
     * Violace : Violacée
     */
    Normal,Pale,Marbre,Violace
}
