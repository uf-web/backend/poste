package com.al.poste.repository;

import com.al.poste.model.Poste;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.relational.core.mapping.Table;
import org.springframework.stereotype.Repository;

//@RepositoryRestResource
@Repository
@Table("poste")
public interface PosteRepository extends JpaRepository<Poste, Integer>, JpaSpecificationExecutor<Poste> {

//    @Query("SELECT p FROM Poste p WHERE p.nom = '1'")
    Page<Poste> findPostesByNom(String username, Pageable pageable);

    Poste findPosteById(Integer id);
}
