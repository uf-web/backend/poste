package com.al.poste.model.bilan;

/**
 * Verbalisation
 */
public enum Verbalisation {
    /**
     * S : Spontanée
     * P : Provoquée
     * A : Absente
     */
    S,P,A
}
