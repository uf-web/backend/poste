package com.al.poste;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

@SpringBootApplication
@EnableDiscoveryClient
public class PosteApplication {

    public static void main(String[] args) {
        SpringApplication.run(PosteApplication.class, args);
    }

}
